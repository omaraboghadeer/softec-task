export interface Users {
    id: string;
    name: string;
    email: string;
    phone: string;
    address: string;
    registerDate: string;
}
