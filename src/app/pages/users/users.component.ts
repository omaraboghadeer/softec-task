import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { UsersService } from './users.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Users } from './users.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {
  displayedColumns = ['id', 'name', 'email', 'phone', 'address', 'registerDate'];
  dataSource = new MatTableDataSource<Users>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private usersSer: UsersService) { }

  ngOnInit() {
    this.getUsers();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
  }

  getUsers() {
    this.usersSer.getUsers().subscribe(res => {
      this.dataSource.data = res as Users[];
    }, err => {
      console.log(err);
    });
  }


}
