import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class OrdersService {
  ordersUrl = '../../../assets/json/Orders.json';

  constructor( private http: HttpClient) {}

  getOrders() {
      return this.http.get<any>(`${this.ordersUrl}`);
  }
}
