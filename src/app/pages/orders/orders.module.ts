import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';

import { OrdersViewComponent } from './orders-view/orders-view.component';
import { OrderDetailsComponent } from './order-details/order-details.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersViewComponent
  },
  {
    pathMatch: 'full',
    path: 'Order/:id/Details',
    component: OrderDetailsComponent
  }
];

@NgModule({
  declarations: [
    OrdersViewComponent,
    OrderDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPaginationModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class OrdersModule { }
