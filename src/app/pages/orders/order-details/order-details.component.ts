import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UsersService } from '../../users/users.service';
import { ProductsService } from '../../products/products.service';


@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  orderId: any;
  order: any;
  userId: any;
  userData: any = [];
  products: any;
  productsData: any = [];

  constructor(private route: ActivatedRoute, private location: Location,
              private userService: UsersService, private productService: ProductsService) {}

  ngOnInit() {
    this.order = this.location.getState();
    this.userId = this.order.userId;
    this.products = this.order.products;
    console.log(this.order);

    this.route.params.subscribe(res => {
      this.orderId = res.id;
    });

    this.getUserData();
    this.getProducts();
  }


  getUserData() {
    if (this.userId) {
      const userData = [];
      this.userService.getUsers().subscribe(res => {
        res.forEach(user => {
          if (user.Id === this.userId) {
            userData.push(user);
          }
        });
        this.userData = userData;
        console.log(this.userData , '<<<<< user data');
      }, err => {
        console.log(err);
      });
    } else {
      alert('No Users');
    }
  }

  getProducts() {
    const productsData = [];
    this.productService.getProducts().subscribe(res => {
      res.forEach(product => {
        for (let i = 0; i < this.products.length; i++) {
          const prod = this.products[i];
          if (product.ProductId ===  prod.ProductId) {
            productsData.push(product);
          }
        }
      });
      this.productsData = productsData;
      console.log(this.productsData, '<<<<< product data');
    }, err => {
      console.log(err);
    });
  }

}
