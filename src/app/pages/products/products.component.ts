import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: any;

  constructor(private productSer: ProductsService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productSer.getProducts().subscribe(res => {
      this.products = res;
    }, err => {
      console.log(err);
    });
  }

}
