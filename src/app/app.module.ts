import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Injector } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Components
import { HeaderComponent } from './components/header/header.component';

// Pages
import { ProductsComponent } from './pages/products/products.component';
import { UsersComponent } from './pages/users/users.component';

// Services
import { ProductsService } from './pages/products/products.service';
import { OrdersService } from './pages/orders/orders.service';

// Angular Material
import { MatIconModule , MatPaginatorModule, MatSortModule, MatTableModule, MatInputModule, MatFormFieldModule  } from '@angular/material';


export let InjectorInstance: Injector;

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    UsersComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxPaginationModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [
    ProductsService,
    OrdersService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [],
})
export class AppModule {
  constructor(private injector: Injector) {
    InjectorInstance = this.injector;
  }
}
