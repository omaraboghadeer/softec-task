import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './pages/products/products.component';
import { UsersComponent } from './pages/users/users.component';
import { OrdersModule } from './pages/orders/orders.module';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'Products',
    pathMatch: 'full'
  },
  {
    path: 'Products',
    component: ProductsComponent
  },
  {
    path: 'Orders',
    loadChildren: './pages/orders/orders.module#OrdersModule'
  },
  {
    path: 'Users',
    component: UsersComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
